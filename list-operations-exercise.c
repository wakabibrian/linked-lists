#include <stdio.h>
#include <stdlib.h>

// Node consists of data and link (pointer to some other node) part (combination of two different parts)
//Self referential structure - creating a node of a single linked list
struct Node
{
    int number;
    struct Node *next;
};

// Function to create a node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Unable to allocate memory for a new node\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
};

// Function to print list
void printList(struct Node *head)
{
    if (head == NULL)
    {
        printf("Linked list is empty.");
    }
    struct Node *current = head;
    printf("[");
    while (current != NULL)
    {
        printf("%d", current->number);
        current = current->next;
        if (current != NULL)
        {
            printf(", ");
        }
    }
    printf("]\n");
};

// Function to append to the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->number = num;
    newNode->next = NULL;
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
};

// Function to prepend to the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    newNode->number = num;
    *head = newNode;
};

// Function to delete by key
void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head;
    struct Node *prev = NULL;
    int position = 0;
    if (*head == NULL)
    {
        printf("List is empty");
        return;
    }
    while (position < key)
    {
        prev = current;
        current = current->next;
        position++;
    }
    prev->next = current->next;
    free(current);
    current = NULL;
};

// Function to delete by value
void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head;
    struct Node *prev = NULL;
    if ((*head) == NULL)
    {
        printf("list is empty");
    }
    if ((*head)->number == value)
    {
        *head = (*head)->next;
        free(current);
        current = NULL;
        printf("Node with value %d deleted\n", value);
        return;
    }
    while (current != NULL && current->number != value)
    {
        prev = current;
        current = current->next;
    }
    if (current == NULL)
    {
        printf("Value %d not found in the list\n", value);
        return;
    }
    prev->next = current->next;
    free(current);
    current = NULL;
    printf("Node with value %d deleted\n", value);
};

// Function to insert after the key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *newNode = createNode(value);
    newNode->number = value;
    newNode->next = NULL;
    struct Node *current = *head;
    struct Node *prev = NULL;

    int position = 0;
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    while (position < key)
    {
        prev = current;
        current = current->next;
        position++;
    }
    current = current->next;
    prev->next = newNode;
    newNode->next = current;
};

// Function to insert after the value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
    struct Node *prev = NULL;
    struct Node *newNode = createNode(newValue);
    newNode->number = newValue;
    newNode->next = NULL;
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    if (current->number != searchValue)
    {
        prev = current;
        current = current->next;
    }
    prev = current;
    current = current->next;
    prev->next = newNode;
    newNode = current;
};

int main()
{
    struct Node *head = NULL;
    int choice, data;

    struct Node *node1 = createNode(23);
    head = node1;
    node1->next = NULL;

    struct Node *node2 = createNode(38);
    node2->next = NULL;
    node1->next = node2;

    struct Node *node3 = createNode(55);
    node3->next = NULL;
    node2->next = node3;

    struct Node *node4 = createNode(65);
    node4->next = NULL;
    node3->next = node4;

    struct Node *node5 = createNode(70);
    node5->next = NULL;
    node4->next = node5;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. PrintList\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;

        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            printList(head);
            break;

        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            printList(head);
            break;

        case 4:
            printf("1.Delete by value.\n");
            printf("2.Delete by key\n");
            printf("Enter your choice\n");
            scanf("%d", &choice);

            switch (choice)
            {

            case 1:
                printf("Enter data to delete: \n");
                scanf("%d", &data);
                deleteByValue(&head, data);
                printList(head);
                break;

            case 2:
                printf("Enter the position where to delete: ");
                scanf("%d", &data);
                deleteByKey(&head, data);
                printList(head);
                break;
            };

        case 5:
            exit(0);

        default:
            printf("Invalid choice. Please try again.\n");
        };
    };
    return 0;
};
